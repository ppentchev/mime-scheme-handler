# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Make sure that `mime-scheme-handler run` starts up at least."""

from __future__ import annotations

import subprocess  # noqa: S404
import sys
import typing

import pytest


if typing.TYPE_CHECKING:
    from typing import Final


def test_run_help_noop() -> None:
    """Make sure that `mime-scheme-handler run --help` and `mime-scheme-handler run --noop` work."""
    output_help: Final = subprocess.check_output(
        [sys.executable, "-m", "mime_scheme_handler", "--help"],  # noqa: S603
        encoding="UTF-8",
    )
    assert "--noop" in output_help
    assert "Would do stuff" not in output_help

    output_noop_test: Final = subprocess.check_output(
        [  # noqa: S603
            sys.executable,
            "-m",
            "mime_scheme_handler",
            "--noop",
            "ringlet-mime-test://nonexistent/thing",
        ],
        encoding="UTF-8",
    )
    assert "--noop" not in output_noop_test
    assert "Would run `x-terminal-emulator -e vi -- /nonexistent/thing`" in output_noop_test


@pytest.mark.parametrize(
    ("url", "expected"),
    [
        (
            "ssh://jrl@nowhere.special:222#A%20comment",
            "--title 'A comment' -e ssh -p 222 -- jrl@nowhere.special",
        ),
        (
            "ssh://someone@someplace",
            "-e ssh -- someone@someplace",
        ),
    ],
)
def test_run_ssh_noop(*, url: str, expected: str) -> None:
    """Make sure the SSH handler generates the correct commands."""
    output: Final = subprocess.check_output(
        [  # noqa: S603
            sys.executable,
            "-m",
            "mime_scheme_handler",
            "--noop",
            url,
        ],
        encoding="UTF-8",
    )
    assert "--noop" not in output
    assert f"Would run `x-terminal-emulator {expected}`" in output
