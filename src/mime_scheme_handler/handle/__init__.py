# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Dispatch the URLs to the requested handlers."""

from __future__ import annotations

import dataclasses
import typing
import urllib.parse

from . import mtest
from . import ssh


if typing.TYPE_CHECKING:
    from typing import Final

    from mime_scheme_handler import defs


@dataclasses.dataclass
class HandleError(Exception):
    """The base class for errors."""


@dataclasses.dataclass
class URLError(HandleError, ValueError):
    """The specified URL did not match the scheme's expectations."""

    url: str
    """The URL we tried to parse."""

    def __str__(self) -> str:
        """Provide a human-readable representation."""
        return f"Invalid URL specified: {self.url!r}"


@dataclasses.dataclass
class NoHostnameError(URLError):
    """There was no hostname in the specified URL."""

    def __str__(self) -> str:
        """Provide a human-readable representation."""
        return f"No hostname in the specified URL: {self.url!r}"


HANDLERS: Final[dict[str, type[defs.SchemeHandler]]] = {
    "ringlet-mime-test": mtest.RingletMimeTestHandler,
    "ssh": ssh.SSHHandler,
}


def get_handler(url: str) -> defs.SchemeHandler:
    """Parse an URL, extract the scheme, look the handler class up."""
    parts: Final = urllib.parse.urlparse(url)
    scheme: Final = parts.scheme
    hclass: Final = HANDLERS[scheme]
    return hclass(scheme=scheme, url=url, parts=parts)
