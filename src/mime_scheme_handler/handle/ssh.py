# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""A sample handler class."""

from __future__ import annotations

import dataclasses
import typing
import urllib.parse

from mime_scheme_handler import defs
from mime_scheme_handler import handle


if typing.TYPE_CHECKING:
    import pathlib
    from typing import Final


@dataclasses.dataclass(frozen=True)
class SSHHandler(defs.SchemeHandler):
    """Somewhat handle the "ssh" scheme."""

    def get_open_command(self) -> list[str | pathlib.Path]:
        """Run a console SSH client via the X terminal emulator."""
        username: Final = self.parts.username
        hostname: Final = self.parts.hostname
        if hostname is None:
            raise handle.NoHostnameError(self.url)

        title_opts: Final = (
            ["--title", urllib.parse.unquote(self.parts.fragment)] if self.parts.fragment else []
        )

        port: Final = self.parts.port
        port_opts: Final = ["-p", str(port)] if port is not None else []

        uhost: Final = f"{username}@{hostname}" if username is not None else hostname
        return ["x-terminal-emulator", *title_opts, "-e", "ssh", *port_opts, "--", uhost]
