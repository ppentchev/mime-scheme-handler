# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""A sample handler class."""

from __future__ import annotations

import dataclasses
import pathlib
import typing

from mime_scheme_handler import defs
from mime_scheme_handler import handle


if typing.TYPE_CHECKING:
    from typing import Final


@dataclasses.dataclass(frozen=True)
class RingletMimeTestHandler(defs.SchemeHandler):
    """Handle the "ringlet-mime-test" sample scheme."""

    def get_open_command(self) -> list[str | pathlib.Path]:
        """Run something via the X terminal emulator."""
        hostname: Final = self.parts.hostname
        if hostname is None:
            raise handle.NoHostnameError(self.url)

        abspath: Final = pathlib.Path(self.parts.path)
        path: Final = abspath.relative_to(abspath.anchor)
        return ["x-terminal-emulator", "-e", "vi", "--", pathlib.Path("/") / hostname / path]
