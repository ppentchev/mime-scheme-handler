# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Do things, or do other things."""

from __future__ import annotations

import shlex
import subprocess  # noqa: S404
import sys
import typing

import click

from . import defs
from . import handle
from . import util


if typing.TYPE_CHECKING:
    from typing import Final


def zen_die(title: str, msg: str) -> None:
    """Display a message using zenity and exit the program."""
    try:
        subprocess.check_call(["zenity", "--error", "--title", title, "--text", msg])  # noqa: S603,S607
    except (OSError, subprocess.CalledProcessError) as err:
        print(f"Could not run zenity: {err}", file=sys.stderr)  # noqa: T201  # report... something.
    sys.exit(msg)


@click.command(name="mime-scheme-handler")
@click.option("--noop", "-N", is_flag=True, help="no-operation mode; display what would be done")
@click.option(
    "--quiet",
    "-q",
    is_flag=True,
    help="quiet operation; only display warning and error messages",
)
@click.option("--verbose", "-v", is_flag=True, help="verbose operation; display diagnostic output")
@click.argument("url", type=str)
def main(*, noop: bool, quiet: bool, verbose: bool, url: str) -> None:
    """Do something, or do something else, who cares anyway."""
    cfg: Final = defs.Config(log=util.build_logger(quiet=quiet, verbose=verbose), verbose=verbose)
    try:
        handler: Final = handle.get_handler(url)
    except KeyError as err:
        zen_die("mime-scheme-handler", f"Unrecognized URL scheme: {err}")
    except ValueError as err:
        zen_die("mime-scheme-handler", f"Could not parse the specified URL: {err}")

    try:
        cmd: Final = handler.get_open_command()
    except handle.HandleError as err:
        zen_die(
            f"mime-scheme-handler: {handler.scheme}",
            f"Could not determine the command to run for the specified URL: {err}",
        )
    cmd_str: Final = shlex.join(str(word) for word in cmd)

    if noop:
        print(f"Would run `{cmd_str}`")  # noqa: T201  # This is the purpose of no-op mode
        return

    cfg.log.debug("Handling '%(url)s' using `%(cmd_str)s`", {"url": url, "cmd_str": cmd_str})
    try:
        subprocess.check_call(cmd)  # noqa: S603  # we hope the handler did it right
    except (OSError, subprocess.CalledProcessError) as err:
        zen_die("mime-scheme-handler: {handler.scheme}", f"Could not run `{cmd_str}`: {err}")


if __name__ == "__main__":
    main()
