# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Common definitions for the mime-scheme-handler library."""

from __future__ import annotations

import dataclasses
import typing


if typing.TYPE_CHECKING:
    import logging
    import pathlib
    import urllib.parse
    from typing import Final


VERSION: Final = "0.1.0"
"""The mime-scheme-handler library version, semver-like."""


@dataclasses.dataclass(frozen=True)
class Config:
    """Runtime configuration for the mime-scheme-handler library."""

    log: logging.Logger
    """The logger to send diagnostic, informational, and error messages to."""

    verbose: bool
    """Verbose operation; display diagnostic output."""


@dataclasses.dataclass(frozen=True)
class SchemeHandler:
    """Run a program to open a URL of the specified scheme."""

    scheme: str
    """The URL scheme that was requested."""

    url: str
    """The URL to open."""

    parts: urllib.parse.ParseResult
    """The URL parsed into parts."""

    def get_open_command(self) -> list[str | pathlib.Path]:
        """Open the URL."""
        raise NotImplementedError(repr(self))
