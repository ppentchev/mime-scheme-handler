<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# mime-scheme-handler - run programs to open URLs

\[[Home][ringlet-mime-scheme-handler] | [GitLab][gitlab] | [PyPI][pypi] | [ReadTheDocs][readthedocs]\]

## Overview

The `mime-scheme-handler` command-line tool can be used to open URLs with custom schemes.
Currently it only recognizes the `ssh://` scheme, and it opens an X terminal emulator to
run an SSH client with the specified connection properties.

## Supported schemes

### SSH

URL format: `ssh://[username@]hostname[:port][#window title]`

The `mime-scheme-handler` tool runs an X terminal to open an SSH connection according to
the parameters specified in the URL:

``` sh
x-terminal-emulator [-title window title] ssh [-p port] -- [username@]hostname
```

## Contact

The `mime-scheme-handler` tool was written by [Peter Pentchev][roam].
It is developed in [a GitLab repository][gitlab]. This documentation is
hosted at [Ringlet][ringlet-mime-scheme-handler] with a copy at [ReadTheDocs][readthedocs].

[roam]: mailto:roam@ringlet.net "Peter Pentchev"
[gitlab]: https://gitlab.com/ppentchev/mime-scheme-handler "The mime-scheme-handler GitLab repository"
[pypi]: https://pypi.org/project/mime-scheme-handler/ "The mime-scheme-handler Python Package Index page"
[readthedocs]: https://mime-scheme-handler.readthedocs.io/ "The mime-scheme-handler ReadTheDocs page"
[ringlet-mime-scheme-handler]: https://devel.ringlet.net/misc/mime-scheme-handler/ "The Ringlet mime-scheme-handler homepage"
